package hu.training360.javasetraining.di;

import hu.training360.javasetraining.di.container.Inject;

public class ParentA {

	@Inject
	private D instanceD;

	public D getInstanceD() {
		return instanceD;
	}

	public void setInstanceD(D instanceD) {
		this.instanceD = instanceD;
	}
	
	
}
