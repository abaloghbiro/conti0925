package hu.training360.javasetraining.di;

import hu.training360.javasetraining.di.container.Inject;

public class A extends ParentA {

	@Inject
	private B instance;

	public IB getInstance() {
		return instance;
	}

}
