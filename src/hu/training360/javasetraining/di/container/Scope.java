package hu.training360.javasetraining.di.container;

public enum Scope {

	SINGLETON, PROTOTYPE;
}
