package hu.training360.javasetraining.di;

import hu.training360.javasetraining.di.container.Inject;

public class B implements IB {

	@Inject
	private C cInstance;

	public C getcInstance() {
		
		return cInstance;
	}

	public void setcInstance(C cInstance) {
		this.cInstance = cInstance;
	}

}
