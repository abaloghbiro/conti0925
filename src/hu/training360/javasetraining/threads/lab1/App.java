package hu.training360.javasetraining.threads.lab1;

public class App {

	public static void main(String[] args) {
		
		Thread t1 = new WorkerThread();
		t1.setName("workerthread1");
		Thread t2 = new Thread(new WorkerRunnable());
		t2.setName("workerthread2");
		t1.start();
		t2.start();

	}

}
