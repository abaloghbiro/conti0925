package hu.training360.javasetraining.threads.lab7;

public class DeadLockExample {

	private Object lock1 = new Object();
	private Object lock2 = new Object();

	public void foo() {
		synchronized (lock1) {
			synchronized (lock2) {
				System.out.println("foo");
			}
		}
	}

	public void bar() {
		synchronized (lock2) {
			synchronized (lock1) {
				System.out.println("bar");
			}
		}
	}

	public static void main(String[] args) {

		DeadLockExample business = new DeadLockExample();

		for (int i = 0; i < 10; i++) {
			new Thread(new Runnable() {
				public void run() {
					business.foo();
				}
			}).start();
		}

		for (int i = 0; i < 10; i++) {
			new Thread(new Runnable() {
				public void run() {
					business.bar();
				}
			}).start();
		}

	}

}