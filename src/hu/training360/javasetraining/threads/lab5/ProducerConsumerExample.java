package hu.training360.javasetraining.threads.lab5;

import java.util.ArrayList;
import java.util.List;

public class ProducerConsumerExample {

	static class Producer implements Runnable {

		private List<Integer> resourcePool;
		private int maxCapacity;

		public Producer(int maxNumberOfResources, List<Integer> resourcePool) {
			this.maxCapacity = maxNumberOfResources;
			this.resourcePool = resourcePool;
		}

		public void produce() {

			synchronized (resourcePool) {

				while (resourcePool.size() == maxCapacity) {

					System.out.println("Resource pool is full " + Thread.currentThread().getName() + "current size: "
							+ resourcePool.size());

					try {
						resourcePool.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

				try {
					Thread.sleep(1000);
					int value = ((int) (Math.random() * 1000 + 1));
					resourcePool.add(value);
					System.out.println("Produced: " + value);
					resourcePool.notifyAll();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		@Override
		public void run() {

			while (true) {
				produce();
			}
		}

	}

	private static class Consumer implements Runnable {

		private List<Integer> resourcePool;

		public Consumer(List<Integer> resourcePool) {
			this.resourcePool = resourcePool;
		}

		public void consume() throws InterruptedException {

			Thread.sleep(2000);

			synchronized (resourcePool) {

				while (resourcePool.isEmpty()) {

					System.out.println("Queue is empty " + Thread.currentThread().getName() + "current size: "
							+ resourcePool.size());

					resourcePool.wait();
				}
				int i = resourcePool.remove(0);
				System.out.println("Consumed: " + i + "/ " + Thread.currentThread().getName());
				resourcePool.notifyAll();
			}
		}

		@Override
		public void run() {

			while (true) {
				try {
					consume();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	public static void main(String[] args) {
		
		List<Integer> taskQueue = new ArrayList<Integer>();
		int MAX_CAPACITY = 5;
		Thread producer = new Thread(new Producer(MAX_CAPACITY,taskQueue), "Producer");
		Thread consumer = new Thread(new Consumer(taskQueue), "Consumer");
		consumer.setPriority(8);
		Thread consumer2 = new Thread(new Consumer(taskQueue), "Consumer2");
		consumer2.setPriority(5);

		producer.start();
		consumer.start();
		consumer2.start();
	}
}
