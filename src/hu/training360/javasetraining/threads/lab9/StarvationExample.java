package hu.training360.javasetraining.threads.lab9;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class StarvationExample {

	public static class Job {

		public synchronized void work() {
			String name = Thread.currentThread().getName();
			String fileName = name + ".txt";

			try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));) {
				writer.write("Thread " + name + " wrote this mesasge");
			} catch (IOException ex) {
				ex.printStackTrace();
			}

			while (true) {
				System.out.println(name + " is working");
			}
		}
	}

	public static void main(String[] args) {
		
		Job job = new Job();

		for (int i = 0; i < 10; i++) {
			new Thread(new Runnable() {
				public void run() {
					job.work();
				}
			}).start();
		}
	}

}