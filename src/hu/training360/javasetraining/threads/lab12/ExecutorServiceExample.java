package hu.training360.javasetraining.threads.lab12;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorServiceExample {

	public void testExecutorServiceWith10ThreadAndRunnable() {

		ExecutorService executorService = Executors.newFixedThreadPool(10);

		executorService.execute(new Runnable() {

			public void run() {
				try {
					Thread.sleep(8000);
					System.out.println("Async task...");
				} catch (InterruptedException e) {
					System.out.println("Task interrupted...");
				}

			}
		});

		executorService.execute(new Runnable() {

			public void run() {
				try {
					Thread.sleep(8000);
					System.out.println("Async2 task...");
				} catch (InterruptedException e) {
					System.out.println("Task interrupted...");
				}

			}
		});

		List<Runnable> tasks = executorService.shutdownNow();

		System.out.println(tasks.size());

	}

	public void testExecutorServiceWith10ThreadAndCallable() throws InterruptedException, ExecutionException {

		ExecutorService executorService = Executors.newFixedThreadPool(10);

		Future<String> task = executorService.submit(new Callable<String>() {

			public String call() throws Exception {
				System.out.println("Important callable task...");
				Thread.sleep(4000);
				System.out.println("Important callable task end...");
				return "important";
			}
		});

		while (!task.isDone()) {

			System.out.println("Waiting for the result!");
			Thread.sleep(1000);
		
			/*
			 * CANCEL TASK boolean res = task.cancel(true);
			 * System.out.println("Task canceled!"+res);
			 */
		}

		System.out.println(task.get());
		executorService.shutdown();

	}

	public void testExecutorServiceWith10ThreadAndCallableInvokeAny() throws InterruptedException, ExecutionException {

		ExecutorService executorService = Executors.newFixedThreadPool(10);

		Set<Callable<String>> callables = new HashSet<Callable<String>>();

		for (int i = 0; i < 10; i++) {

			final int c = i;
			callables.add(new Callable<String>() {

				public String call() throws Exception {
					long sleep = (long)(Math.random()*2000+1);
					System.out.println("Important callable task..."+c + " / "+sleep);
					Thread.sleep(sleep);
					return new Integer(c).toString();
				}
			});
		}

		String result = executorService.invokeAny(callables);

		executorService.shutdown();

		System.out.println(result);

	}

	public void testExecutorServiceWith10ThreadAndCallableInvokeAll() throws InterruptedException, ExecutionException {

		ExecutorService executorService = Executors.newFixedThreadPool(10);

		Set<Callable<String>> callables = new HashSet<Callable<String>>();

		for (int i = 0; i < 20; i++) {

			final int c = i;
			callables.add(new Callable<String>() {

				public String call() throws Exception {
					long sleep = (long)(Math.random()*2000+1);
					System.out.println("Important callable task..."+c + " / "+sleep);
					Thread.sleep(sleep);
					return new Integer(c).toString();
				}
			});
		}
		List<Future<String>> result = executorService.invokeAll(callables);
	

		executorService.shutdown();

		List<String> results = new ArrayList<>();
		
		for (Future<String> f : result) {

			System.out.println(f.isDone());
			results.add(f.get());
		}
		
		System.out.println(results);

	}
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {

		ExecutorServiceExample e = new ExecutorServiceExample();

		e.testExecutorServiceWith10ThreadAndCallableInvokeAll();

	}

}
