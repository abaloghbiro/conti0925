package hu.training360.javasetraining.generics2;

public class GenericMethodDemo {

	public static <T extends Number> void inspect(T object) {
		System.out.println("T object class " + object.getClass());
		System.out.println("T object: " + object);
	}
	
	
	public static void main(String[]args) {
		
		
		GenericMethodDemo.inspect(3);
		GenericMethodDemo.inspect(4L);
		GenericMethodDemo.inspect(3.14D);
		
	}
}
