package hu.training360.javasetraining.generics;


public interface AccountRepository extends CrudRepository<Long,Account> {
	Account findByAccountName(String accountName);
}
