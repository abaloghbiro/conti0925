package hu.training360.javasetraining.java8;

@FunctionalInterface
public interface Rowmapper<S, T> {

	T map(S object);

}
